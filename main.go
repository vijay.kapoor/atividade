package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

type User struct {
	Email  string `json:"email"`
	Pass   string `json:"pass"`
	Birthdate string `json:"birthdate"`
}

type UserLogged struct {
	Email string `json:"email"`
	Pass string `json:"pass"`
}

type Payload struct {
	Message string `json:"message"`
}

var s = make([]string, 0, 3)

func main() {
	defer StartServer()

	log.Println("[INFO] Servidor no ar na porta :5000")
}

func StartServer() {
	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/register", Register).Methods("POST")
	r.HandleFunc("/login", Login).Methods("POST")

	server := &http.Server{
		Addr       : "localhost:5000",
		IdleTimeout: duration,
		Handler    : r,
	}

	log.Fatal(server.ListenAndServe())
}

func Register(res http.ResponseWriter, req *http.Request) {

	var u User

	if req.Method != "POST" {
		res.WriteHeader(http.StatusMethodNotAllowed)
		res.Write([]byte(""))
		return
	}

	if req.Header.Get("Content-Type") != "application/json" {
		res.WriteHeader(http.StatusBadRequest)
		payload, _ := json.Marshal(Payload{Message: "invalid header"})
		res.Write([]byte(payload))
		return
	}

	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)

	if len(u.Email) == 0 || len(u.Pass) == 0 || len(u.Birthdate) == 0 {
		res.WriteHeader(http.StatusBadRequest)
		payload, _ := json.Marshal(Payload{Message: "invalid body"})
		res.Write([]byte(payload))
		return
	}

	s = append(s, u.Email)
	s = append(s, u.Pass)
	s = append(s, u.Birthdate)

	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")

	payload, _ := json.Marshal(Payload{Message: "user successfully registered"})
	res.Write([]byte(payload))
}

func Login(res http.ResponseWriter, req *http.Request) {

	var u UserLogged

	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)

	if req.Method != "POST" {
		res.WriteHeader(http.StatusMethodNotAllowed)
		res.Write([]byte(""))
		return
	}

	if req.Header.Get("Content-Type") != "application/json" {
		res.WriteHeader(http.StatusBadRequest)
		payload, _ := json.Marshal(Payload{Message: "invalid header"})
		res.Write([]byte(payload))
		return
	}

	if len(u.Email) == 0 || len(u.Pass) == 0 {
		res.WriteHeader(http.StatusBadRequest)
		payload, _ := json.Marshal(Payload{Message: "invalid body"})
		res.Write([]byte(payload))
		return
	}

	if u.Email != s[0] || u.Pass != s[1] {
		res.WriteHeader(http.StatusForbidden)
		res.Write([]byte(""))
		return
	}

	b, _ := time.Parse("02/01/2006", s[2])
	df := strconv.FormatInt(int64(time.Now().Year() - b.Year()), 10)

	res.WriteHeader(http.StatusOK)
	payload, _ := json.Marshal(Payload{Message: "user successfully login and has " + df + " years old"})
	res.Write([]byte(payload))
}